package com.example.work_project;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LogOut_sharepreparance extends AppCompatActivity {
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_out_sharepreparance);
        Button Login = (Button)findViewById(R.id.loginBtn);

        sp = getSharedPreferences("login",MODE_PRIVATE);

        /*if(sp.getBoolean("logged",true)){
            goToMainActivity();
        }*/

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                goToMainActivity();
                sp.edit().putBoolean("logged",false).apply();
            }
        });


    }

    public void goToMainActivity(){
        Intent i = new Intent(this,Login_sharepreparance.class);
        startActivity(i);
    }


}
