package com.example.work_project;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Login_sharepreparance extends AppCompatActivity {
    EditText user;
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_sharepreparance);
        Button Login = (Button)findViewById(R.id.loginBtn);
        EditText editText=(EditText)findViewById(R.id.edit);
        sp = getSharedPreferences("login",MODE_PRIVATE);
      final String ed=  editText.getText().toString();

        if (
               sp.getBoolean("logged",false))

                {
                    goToMainActivity();
                }

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainActivity();
                //sp.getString("name",""+ed+"");
                sp.edit().putBoolean("logged",true).apply();
            }
        });


    }

    public void goToMainActivity(){
        Intent i = new Intent(this,LogOut_sharepreparance.class);
        startActivity(i);
    }


}
