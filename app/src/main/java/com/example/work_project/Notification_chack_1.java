package com.example.work_project;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Notification_chack_1 extends AppCompatActivity {


    public static final String CHANNEL_ID ="channel_id01" ;
    public static final int NOTIFICATION_ID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_chack_1);
    }

    public void submit(View view) {
        SubmitNotification();

    }

    private void SubmitNotification() {
        createNotificationChannel();
        //Notification Action
        Intent mainIntent=new Intent(this,Notification_chack_1.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent= (PendingIntent) PendingIntent.getActivity(this,0,mainIntent,
                PendingIntent.FLAG_ONE_SHOT);
        //Click Like Button LikeActivity

        Intent LikeIntent=new Intent(this,Notification_like_action.class);
        LikeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent LikePIntent= (PendingIntent) PendingIntent.getActivity(this,0,LikeIntent,
                PendingIntent.FLAG_ONE_SHOT);
        //Click Like Button DislikeActivity

        Intent disLikeIntent=new Intent(this,Notification_Dislike_action.class);
        disLikeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent DislikePIntent= (PendingIntent) PendingIntent.getActivity(this,0,disLikeIntent,
                PendingIntent.FLAG_ONE_SHOT);

///Close The Notification Action

        NotificationCompat.Builder builder= new NotificationCompat.Builder(this,CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_notifications_black_24dp);
        builder.setContentTitle("Update the Apps");
        builder.setContentText("The Apps Latest Version Pleces Update Apps");
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        //Dismiss OnTap
        builder.setAutoCancel(true);

        //Start Intant Notification  Tap (Notification_chack_1)
        builder.setContentIntent(pendingIntent);
        //add Action Button to Notification
        builder.addAction(R.drawable.ic_thumb_up_black_24dp,"Like",LikePIntent);
        builder.addAction(R.drawable.ic_thumb_down_black_24dp,"Dislike",DislikePIntent);

        //Notification Image Show
        Bitmap bitmap= BitmapFactory.decodeResource(getResources(),R.drawable.ic_wifi_black_24dp);
        builder.setLargeIcon(bitmap);
        builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap).bigLargeIcon(null));

        //Notification Big Description
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText("\"https://gitlab.com/Basu0/work_project/commits/master\";"));


        NotificationManagerCompat notificationManagerCompat=NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());
    }

    private void createNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            CharSequence name="MY Notification";
            String description="https://gitlab.com/Basu0/work_project/commits/master";
            int importance=NotificationManager.IMPORTANCE_DEFAULT;

                NotificationChannel notificationChannel= new NotificationChannel(CHANNEL_ID, name, importance);
                notificationChannel.setDescription(description);
                NotificationManager notificationManager=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }

    }

