package com.example.work_project;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Valu_Show_intant extends AppCompatActivity {
 TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valu__show_intant);
        textView=(TextView)findViewById(R.id.textshow);
        Bundle bundle=getIntent().getExtras();
        if (bundle!=null){
            String value=bundle.getString("tag");
            textView.setText(value);
        }
    }
}
