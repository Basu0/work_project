package com.example.work_project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Value_pass_other_intant extends AppCompatActivity {
 EditText editText;
 Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_value_pass_other_intant);
        editText=(EditText)findViewById(R.id.editid);
        button=(Button)findViewById(R.id.btid);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valu=editText.getText().toString();
                Intent intent=new Intent(Value_pass_other_intant.this,Valu_Show_intant.class);
                intent.putExtra("tag",valu);
                startActivity(intent);

            }
        });
    }
}
