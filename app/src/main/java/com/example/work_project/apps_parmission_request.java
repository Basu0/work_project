package com.example.work_project;

import android.Manifest;
import android.app.Application;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class apps_parmission_request extends AppCompatActivity implements EasyPermissions.PermissionCallbacks{
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.apps_parmission_request);
        button=(Button)findViewById(R.id.permisss);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camraRuntime();
            }
        });
    }
   @AfterPermissionGranted(123)
    private void camraRuntime() {
        String[] permis={Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this,permis)){
            Toast.makeText(this,"Camra Open",Toast.LENGTH_LONG).show();

        }
        else {
            EasyPermissions.requestPermissions(this,"Please Allow Permission",123,permis);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults,this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        Toast.makeText(this,"Camra Working",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this,perms)){
            new AppSettingsDialog.Builder(this).build().show();
            //new AppSettingsDialog.Builder(this).build().show();
        }

    }
}
