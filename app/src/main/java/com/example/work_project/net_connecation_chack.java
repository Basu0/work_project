package com.example.work_project;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class net_connecation_chack extends AppCompatActivity {
  ImageView imageView;
  Button button;
  TextView texk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.net_connecation_chack);
        imageView=(ImageView) findViewById(R.id.imageid);
        button=(Button)findViewById(R.id.btonconnec);
        texk=(TextView)findViewById(R.id.texidy);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChackInnetwork();
            }
        });

    }

    private void ChackInnetwork() {
        boolean wificonneaction;
        boolean mobileconneaction;
        ConnectivityManager connectivityManager=(ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();
        if (networkInfo !=null && networkInfo.isConnected()){
            wificonneaction=networkInfo.getType()==ConnectivityManager.TYPE_WIFI;
            mobileconneaction=networkInfo.getType()==ConnectivityManager.TYPE_MOBILE;

            if (wificonneaction){
                imageView.setImageResource(R.drawable.ic_wifi_black_24dp);
                texk.setText("Connecation Wifi");

            }
            else if (mobileconneaction){
                imageView.setImageResource(R.drawable.ic_swap_vert_black_24dp);
                texk.setText("Connecation Mobile Data");

            }



          }else {
            imageView.setImageResource(R.drawable.ic_do_not_disturb_black_24dp);
            texk.setText("No Internet Connecation ");

        }
    }
}
