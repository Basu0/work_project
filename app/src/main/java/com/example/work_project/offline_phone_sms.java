package com.example.work_project;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class offline_phone_sms extends AppCompatActivity {
 private EditText api,sendar,phone,messageg;
 private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_phone_sms);

        api=(EditText)findViewById(R.id.apikey);
        sendar=(EditText)findViewById(R.id.sendar);
        phone=(EditText)findViewById(R.id.phone);
        messageg=(EditText)findViewById(R.id.message);
        button=(Button)findViewById(R.id.btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendsms();
            }
        });

    }


    public void sendsms(){
        String phonen=phone.getText().toString().trim();
        String sms=messageg.getText().toString();
        if (phonen==null || phonen.equals("")||sms==null||sms.equals("")){
            Toast.makeText(getApplication(),"Field Empty",Toast.LENGTH_LONG).show();
        }

        else {

            if (TextUtils.isDigitsOnly(phonen)){

                SmsManager smsManager=SmsManager.getDefault();
                smsManager.sendTextMessage(phonen,null,sms,null,null);
                Toast.makeText(getApplication(),"Sms Send Successfullay",Toast.LENGTH_LONG).show();
            }
            else {

                Toast.makeText(getApplication(),"Plz Entar int only",Toast.LENGTH_LONG).show();
            }
        }
    }
}
