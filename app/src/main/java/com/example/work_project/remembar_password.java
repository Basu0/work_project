package com.example.work_project;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

public class remembar_password extends AppCompatActivity implements TextWatcher, CompoundButton
.OnCheckedChangeListener{
    EditText editText1,editText2;
    CheckBox checkBox;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private static  final String PREF_NAME="prefs";
    private static  final String KEY_REMEMBER="remember";
    private static  final String KEY_USERNAME="username";
    private static  final String KEY_PASSWORD="password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remembar_password);
        sharedPreferences=getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        editText1=(EditText)findViewById(R.id.emailid);
        editText2=(EditText)findViewById(R.id.passwodid);
        checkBox=(CheckBox)findViewById(R.id.chackbox);
        if (sharedPreferences.getBoolean(KEY_REMEMBER,false))
            checkBox.setChecked(true);
        else
            checkBox.setChecked(false);
        editText1.setText(sharedPreferences.getString(KEY_USERNAME,""));
        editText2.setText(sharedPreferences.getString(KEY_PASSWORD,""));
        editText1.addTextChangedListener(this);
        editText2.addTextChangedListener(this);
        checkBox.setOnCheckedChangeListener(this);

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        managefre();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        managefre();

    }

    private void managefre(){
        if (checkBox.isChecked()){
            editor.putString(KEY_USERNAME,editText1.getText().toString().trim());
            editor.putString(KEY_PASSWORD,editText2.getText().toString().trim());
            editor.putBoolean(KEY_REMEMBER,true);
            editor.apply();
        }
        else {
            editor.putBoolean(KEY_REMEMBER,false);
            editor.remove(KEY_PASSWORD);
            editor.remove(KEY_USERNAME);
            editor.apply();


        }
    }
}
