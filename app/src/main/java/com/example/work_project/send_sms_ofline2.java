package com.example.work_project;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class send_sms_ofline2 extends AppCompatActivity {
    private EditText api,sendar,phone,messageg;
    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms_ofline2);
        api=(EditText)findViewById(R.id.apikey);
        sendar=(EditText)findViewById(R.id.sendar);
        phone=(EditText)findViewById(R.id.phone);
        messageg=(EditText)findViewById(R.id.message);
        button=(Button)findViewById(R.id.btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendms();
            }
        });
    }

    public void sendms(){

        try {
            // Construct data
            String apiKey = "apikey=" +api.getText().toString();
            String message = "&message=" + messageg.getText().toString();
            String sender = "&sender=" + sendar.getText().toString();
            String numbers = "&numbers=" + phone.getText().toString();

            // Send data
            HttpURLConnection conn = (HttpURLConnection) new URL("https://api.txtlocal.com/send/?").openConnection();
            String data = apiKey + numbers + message + sender;
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                //stringBuffer.append(line);
                Toast.makeText(getApplication(),"Success"+line,Toast.LENGTH_LONG).show();
            }
            rd.close();

            //return stringBuffer.toString();
        } catch (Exception e) {
            //System.out.println("Error SMS "+e);
            //return "Error "+e;
            Toast.makeText(getApplication(),"Exception"+e,Toast.LENGTH_LONG).show();
        }
    }
}
